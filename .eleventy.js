module.exports = function (eleventyConfig) {
    // Copy the `css` folders to the output
    eleventyConfig.addPassthroughCopy("src/css");
    return {
        markdownTemplateEngine: "njk",
        htmlTemplateEngine: "njk",
        pathPrefix: "/11ty-test",
        dir: {
            input: "./src",
            includes: "_includes",
            data: "_data",
            output: "public"
        }
    }
}